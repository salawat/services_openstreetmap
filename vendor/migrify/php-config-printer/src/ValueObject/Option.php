<?php

declare(strict_types=1);

namespace Migrify\PhpConfigPrinter\ValueObject;

final class Option
{
    /**
     * @api
     * @var string
     */
    public const INLINE_VALUE_OBJECT_FUNC_CALL_NAME = 'inline_value_object_func_call_name';
}
